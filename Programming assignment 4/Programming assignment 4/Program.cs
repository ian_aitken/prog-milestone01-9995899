﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_assignment_4
{
    class Program
    {
        static void GetChoice(ref int ichoice)
        {
            Console.Write("Enter choice: ");
            string input = Console.ReadLine();

            bool result = int.TryParse(input, out ichoice);

            if (!result)
            {
                while (!result && ichoice > 3)
                {
                    Console.WriteLine("Invalid value.Try again:");
                    input = Console.ReadLine();
                    result = int.TryParse(input, out ichoice);
                }
            }
        }

        static void Main(string[] args)
        {
            double fTemp;
            double cTemp;
            double convertC;
            double convertF;
            

            Console.WriteLine("Welcome to the temperature conversion application");
            Console.WriteLine("_________________________________________________");
            Console.WriteLine("1. Fahrenheit to Celsius");
            Console.WriteLine("2. Celsius to Fahrenheit");
            Console.WriteLine("3. Exit");

            int ichoice = 0;
            GetChoice(ref ichoice);
            do
            {

                if (ichoice == 1)
                {
                    Console.WriteLine("Enter Fahrenheit temperature: ");
                    fTemp = int.Parse(Console.ReadLine());
                    convertC = ConvertCelcius(fTemp);
                    Console.WriteLine(fTemp + "Fahrenheit is " + convertC + "Celsius");
                    Console.WriteLine("Welcome to the temperature conversion application");
                    Console.WriteLine("_________________________________________________");
                    GetChoice(ref ichoice);
                }
                if (ichoice == 2)
                {
                    Console.WriteLine("Enter Celsius temperature: ");
                    cTemp = int.Parse(Console.ReadLine());
                    convertF = ConvertFahrenheit(cTemp);
                    Console.WriteLine(cTemp + "Celsius is " + convertF + "Fahrenheit");
                    Console.WriteLine("Welcome to the temperature conversion application");
                    Console.WriteLine("____________________________________________________");
                    GetChoice(ref ichoice);

                }
                if (ichoice == 3)
                {
                    Console.WriteLine("Thank you for using the temperature conversion application. Please come again.");
                }
                else
                {
                    Console.WriteLine("Invalid choice. Please choose again!");
                }

            }
            while (ichoice < 3);

        }
        static double ConvertCelcius(double c)
        {
            double f;

            return f = 9.0 / 5.0 * (c + 32);
        }
        static double ConvertFahrenheit(double f)
        {
            double c;

            return c = 5.0 / 9.0 * (f - 32);

        }
    }

}
