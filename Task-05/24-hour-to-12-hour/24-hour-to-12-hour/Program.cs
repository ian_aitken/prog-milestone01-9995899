﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _24_hour_to_12_hour
{
    class Program
    {
        static void Main(string[] args)
        { 
            Console.WriteLine("What is the time: ");
            var hour = 12;
            var time = int.Parse(Console.ReadLine());
            Console.Clear();


            if (time > 12)
            {
                var timehour = time - hour;
                Console.WriteLine($"The time is {timehour}:00 pm");
            }

            else
            {
                Console.WriteLine($"The time is {time}:00 am");
            }
            if (time > 24)
            {
                Console.Clear();
                Console.WriteLine("Invalid Time Please Input correct time below 24");
            }
            
        }
    }
}
