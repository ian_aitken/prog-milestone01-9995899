﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_12
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            Console.WriteLine("enter a number : ");
            i = int.Parse(Console.ReadLine());
            if (i % 2 == 0)
            {
                Console.WriteLine("Entered Number is an Even Number");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Entered Number is an Odd Number");
                Console.ReadLine();
            }
        }
    }
}
