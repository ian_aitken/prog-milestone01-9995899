﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                int num, i, result;
                Console.Write("Enter a number\t");
                num = Convert.ToInt32(Console.ReadLine());

                for (i = 1; i <= 12; i++)
                {
                    result = num * i;
                    Console.WriteLine("{0} x {1} = {2}", num, i, result);
                }
                Console.ReadLine();
            }
        }
    }
}
